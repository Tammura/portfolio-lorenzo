# Portfolio

My personal website, featuring projects that I've worked at.

[![Netlify Status](https://api.netlify.com/api/v1/badges/f5bd4038-68b5-4ffc-ba5e-d331dc060ee4/deploy-status)](https://app.netlify.com/sites/lorenzo-capalbo/deploys)

## Requirements

You'll need the following dependencies in order to build the website:
- Hugo
- Yarn

## Installation

After cloning the repo, you have to generate the Tailwind CSS styles:
```bash
$ cd themes/portfolio
$ yarn install
$ yarn build
```

Then, you can build the website from the root folder:
```bash
$ hugo
```

The output will appear in the public folder.

## Roadmap

- [x] Homepage
- [x] 404 page
- [ ] Projects page

## Contributing

Feel free to open any issue [here](https://gitlab.com/KLB0/portfolio/-/issues).

## Acknowledgment

Huge thanks to the following projects, which have made mine possible:
- [Feather icons](https://feathericons.com/)
- [Hugo](https://gohugo.io/)
- [Netlify](https://www.netlify.com/)
- [TailwindCSS](https://tailwindcss.com/)

## License

[MIT](https://choosealicense.com/licenses/mit/)
